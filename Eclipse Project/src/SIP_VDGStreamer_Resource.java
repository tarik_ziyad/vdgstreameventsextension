import com.thingworx.datatables.DataTableThing;
import com.thingworx.entities.RootEntity;
import com.thingworx.entities.utils.EntityUtilities;
import com.thingworx.entities.utils.ThingUtilities;
import com.thingworx.logging.LogUtilities;
import com.thingworx.metadata.annotations.*;
import com.thingworx.persistence.TransactionFactory;
import com.thingworx.relationships.RelationshipTypes.ThingworxRelationshipTypes;
import com.thingworx.resources.Resource;
import com.thingworx.security.context.SecurityContext;
import com.thingworx.security.permissions.PermissionTypes;
import com.thingworx.security.permissions.RunTimePermissionCollection;
import com.thingworx.streams.StreamThing;
import com.thingworx.things.Thing;
import com.thingworx.things.properties.ThingProperty;
import com.thingworx.things.repository.FileRepositoryThing;
import com.thingworx.thingshape.ThingShape;
import com.thingworx.thingtemplates.ThingTemplate;
import com.thingworx.types.ConfigurationTable;
import com.thingworx.types.InfoTable;
import com.thingworx.types.collections.ValueCollection;
import com.thingworx.types.collections.ValueCollectionList;
import com.thingworx.types.primitives.*;
import com.thingworx.webservices.context.ThreadLocalContext;
import org.apache.commons.codec.binary.Base64;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;


public class SIP_VDGStreamer_Resource extends Resource {

    //This represents the API URL where the evens come from
    private static String Vdg_apiURL = "http://192.168.1.70/command?command=streamEvents";
    //The user name to authenticate with VDG server (basic authentication)    
    private static String Vdg_userName = "dvm";
    //The password to authenticate with VDG server (basic authentication)
    private static String Vdg_Password = "dvm2018";
    /*The threshold in minutes when we consider the Sensit’s BayOverstay an alarm
    The VDG system is connected with the Sensit, so latter sends it’s event to the VDG
    we update the integration, so we can get the Sensit’s event only from sensit
    So, this line can be ignored*/
    private static int overStayThreshold = 0; //in minutes
    //initialize a TW logger 
    protected static Logger _logger = LogUtilities.getInstance().getApplicationLogger(SIP_VDGStreamer_Resource.class);
    //not used
    static FileRepositoryThing SystemRepository;
    //   array to represents if I received this kind of alarm it is red, yellow or green
    private static ArrayList<Alarm> alarm_codes;
    //reference to the PSIM_RedAlarms_DataTable
    private static DataTableThing redAlamsDT;
    //reference to the PSIM_YellowAlarms_DataTable
    private static DataTableThing yellowAlamsDT;
    //reference to the PSIM_GreenAlarms_DataTable
    private static DataTableThing greenAlamsDT;
    //reference to the VDGEventStream
    private static StreamThing VDGEventStream;
    //Boolean variable to store the status of the connection
    private static boolean isConnected = true;
    //the number of rows in the PSIM_AlarmCodes_DataTable
    private static int alarmsCodeLength = 0;
    //connection object to the VDG
    private static Connection c = null;
    //created statement from the connection object
    private static Statement statement = null;
    //native SQL command string
    private static String sql;
    //buffer reader object 
    private static BufferedReader in;
    //private static InfoTable deployedStations = null;

    /*this method must be implemented when we derive from Resource class,
    it is called automatically when the TW environment is start working, 
    so we can start stream the events.*/
    public void initializeEntity() throws Exception {

        super.initializeEntity();
        //write some logs
        _logger.info("SIP_VDGStreamer_Resource is initialized");
        isConnected = true; // the first time the extension works, it should be connected
        try {
            //Reference to the PSIM_AlarmManagementService_Thing 
            Thing AlarmManagementServices_Thing = (Thing) EntityUtilities.findEntityDirect("PSIM_AlarmManagementServices_Thing",
                    ThingworxRelationshipTypes.Thing);
            //If the thing isn’t started yet, start it
            if (!AlarmManagementServices_Thing.isRunning()) {
                AlarmManagementServices_Thing.EnableThing();
                AlarmManagementServices_Thing.RestartThing();
            }
            //IPrimitiveType ff = AlarmManagementServices_Thing.getPropertyValue("OverstayMinutesLimit");
            //Read the over stay Threshold settings from the thing property
            overStayThreshold = ((IntegerPrimitive) AlarmManagementServices_Thing.getPropertyValue("OverstayMinutesLimit")).getValue();

            /*open new thread for working, if you do not open new thread, when the extensions imported 
            the main TW thread will be stop responding to the composer UI, but the event streaming will start coming */
            Thread t = new Thread(() -> {
                try {
                    //The main method for start streaming the events
                    StreamEvents();

                    _logger.error("SIP_VDGStreamer_Resource wait 10 seconds .....");
                } catch (Exception e) {
                    _logger.error("SIP_VDGStreamer_Resource Error: " + e.getMessage());
                    _logger.error(String.valueOf(e.getStackTrace()[0].getLineNumber()));
                    _logger.error("SIP_VDGStreamer_Resource wait 10 seconds .....");
                    try {
                        //if some problem happens, wait 10 seconds and retry connecting again in endless manner 
                        TimeUnit.SECONDS.sleep(10);
                        //Re calling the streamEvents method
                        StreamEvents();
                    } catch (Exception e2) {

                    }
                }

            });
            t.start();


        } catch (Exception e) {
            _logger.error("SIP_VDGStreamer_Resource Error: " + e.getMessage());
            _logger.error(String.valueOf(e.getStackTrace()[0].getLineNumber()));
            _logger.error("SIP_VDGStreamer_Resource wait 10 seconds .....");
            TimeUnit.SECONDS.sleep(10);
            this.StreamEvents();
        }

    }

    /*This method is responsible for fetching the data in the 
    PSIM_AlarmCodes_DataTable, where the events types and the events alarm colors is stored,
    parse the result and store it into alarm_codes arrayList object.
    This data table contains data like: EventType1 is red, yellow or green …
    */
    public void getRedYellowGreenEvents() throws Exception {
        try {
            //This data table contains data like: Code:EventType1 , Description:red, yellow or green …
            alarm_codes = new ArrayList<Alarm>();

            DataTableThing alarmCodesDT = (DataTableThing) EntityUtilities.findEntityDirect("PSIM_AlarmCodes_DataTable",
                    ThingworxRelationshipTypes.Thing);

            //may the resource will start before the Data tables things
            // so enable the DT thing before accessing it's data
            if(!alarmCodesDT.isRunning()) {
                alarmCodesDT.EnableThing();
                alarmCodesDT.RestartThing();
            }

            alarm_codes.clear();
            double noRows = 500;
            //fetch all entries of the PSIM_AlarmCodes_DataTable
            InfoTable AlarmCodes = alarmCodesDT.GetDataTableEntries(noRows);
            //get the returned rows count
            int AlarmCodesLength = AlarmCodes.getRowCount();
            //loop the entries
            for (int i = 0; i < AlarmCodesLength; i++) {
                //fill the row data into Alarm object
                ValueCollection alarm = AlarmCodes.getRow(i);
                Alarm alarm_obj = new Alarm();
                alarm_obj.setDescription(alarm.getStringValue("Alarm_Description").trim());
                alarm_obj.setCode(alarm.getStringValue("Alarm_Code").trim());
                alarm_codes.add(alarm_obj);
            }
            //update the row count, I don't remember if this is required
            alarmsCodeLength = alarm_codes.size();

        } catch (Exception e) {
            _logger.error("SIP_VDGStreamer_Resource Error: " + e.getMessage());
            _logger.error(String.valueOf(e.getStackTrace()[0].getLineNumber()));
        }
    }
    /*This method is responsible for getting the:
    jdbcUrl, username and password for the TW persistence provider.
    Also, initializing the class variables:
     VDGEventStream, redAlamsDT, yellowAlamsDT and greenAlamsDT  */
    public void InitializeExtensionVariables() {
        try {

            //get Reference to the TW Persistence Provider, this is used to send SQL command to the DB using TW,
            //using this approach instead of calling the AddDateTableEntry service is because the latter one doesn't work
            // while the AddStreamEntry is working
            RootEntity twPersistenceProvider = (RootEntity) EntityUtilities.findEntityDirect("ThingworxPersistenceProvider",
                    ThingworxRelationshipTypes.PersistenceProvider);
            //get the configuration table of the persistence provider
            ConfigurationTable configTable = (ConfigurationTable) (twPersistenceProvider).GetConfigurationTable("ConnectionInformation");
            ValueCollection valueCollection = configTable.getRow(0);
            //get the jdbc url to connect to DB
            String jdbcUrl = (String) valueCollection.getValue("jdbcUrl");
             //get the username to connect to DB
            String username = (String) valueCollection.getValue("username");
             //get the password to connect to DB
            String Password = (String) valueCollection.getValue("password");


            alarm_codes = new ArrayList<Alarm>();

            Class.forName("org.postgresql.Driver");
            //initialize the connection variable
            c = DriverManager.getConnection(jdbcUrl, username, Password);
            c.setAutoCommit(false);
            statement = c.createStatement();

            //assign reference tı the VDGEventStream Entity
            VDGEventStream = (StreamThing) EntityUtilities.findEntityDirect("VDGEventStream",
                    ThingworxRelationshipTypes.Thing);
            //assign reference tı the PSIM_RedAlarms_DataTable Entity
            redAlamsDT = (DataTableThing) EntityUtilities.findEntityDirect("PSIM_RedAlarms_DataTable",
                    ThingworxRelationshipTypes.Thing);
            //assign reference tı the PSIM_YellowAlarms_DataTable Entity
            yellowAlamsDT = (DataTableThing) EntityUtilities.findEntityDirect("PSIM_YellowAlarms_DataTable",
                    ThingworxRelationshipTypes.Thing);
            //assign reference tı the PSIM_GreenAlarms_DataTable Entity
            greenAlamsDT = (DataTableThing) EntityUtilities.findEntityDirect("PSIM_GreenAlarms_DataTable",
                    ThingworxRelationshipTypes.Thing);
            //assign reference tı the DVM_SiteManagement_StationTemplate Entity
            ThingTemplate DVM_SiteManagement_MasterThing = (ThingTemplate) EntityUtilities.findEntityDirect("DVM_SiteManagement_StationTemplate",
                    ThingworxRelationshipTypes.ThingTemplate);

            //if one of the entities is not running, run it
            if (!VDGEventStream.isRunning()) {
                VDGEventStream.EnableThing();
                VDGEventStream.RestartThing();
            }
            if (!redAlamsDT.isRunning()) {
                redAlamsDT.EnableThing();
                redAlamsDT.RestartThing();
            }
            if (!yellowAlamsDT.isRunning()) {
                yellowAlamsDT.EnableThing();
                yellowAlamsDT.RestartThing();
            }
            if (!greenAlamsDT.isRunning()) {
                greenAlamsDT.EnableThing();
                greenAlamsDT.RestartThing();
            }// PSIM_AlarmManagementServices_Thing


            //deployedStations = DVM_SiteManagement_MasterThing.GetImplementingThingsWithData();


        } catch (Exception e) {
            _logger.error("SIP_VDGStreamer_Resource Error: " + e.getMessage());
            _logger.error("SIP_VDGStreamer_Resource Error LineNumber:" + String.valueOf(e.getStackTrace()[0].getLineNumber()));
        }
    }

    /*This method initializes the connection to the VDG server
     and calls the Read method to start parsing the events */
    public void StreamEvents() throws Exception {
        try {
            if (isConnected) {

                this.InitializeExtensionVariables();
                this.getRedYellowGreenEvents();

                // thingworx healthcare yapilacak
                
                //initialize the connection  params to the API
                String webPage = Vdg_apiURL;
                String name = Vdg_userName;
                String password = Vdg_Password;
                String authString = name + ":" + password;
                byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
                String authStringEnc = new String(authEncBytes);
                URL url = new URL(webPage);
                URLConnection urlConnection = url.openConnection();
                urlConnection.setConnectTimeout(10000);
                urlConnection.setReadTimeout(120000);
                urlConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);
                this.Read(urlConnection);

            }
        } catch (Exception e) {
            _logger.error("SIP_VDGStreamer_Resource Error: " + e.getMessage());
            _logger.error(String.valueOf(e.getStackTrace()[0].getLineNumber()));
            _logger.error("SIP_VDGStreamer_Resource wait 10 seconds .....");
            //if there is a problem happend, wait 10 seconds and re-connect again
            TimeUnit.SECONDS.sleep(10);
            this.StreamEvents();
        }

    }

    private void Read(URLConnection urlConnection) throws Exception {


        try {
            //write some logs to TW
            _logger.info("SIP_VDGStreamer_Resource Info: Current Thread in run: "
                    + Thread.currentThread().getName());

            TransactionFactory.beginTransactionRequired();
            //if you don't write this line, you will have an exception in TW
            ThreadLocalContext.setSecurityContext(SecurityContext.createSystemUserContext());

            //parse the coming XML doc
            in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String ContentType = urlConnection.getHeaderField(1).toString();
            int boundaryStartIndex = ContentType.indexOf("boundary=") + 9;
            String boundary = ContentType.substring(boundaryStartIndex);
            int count = 0;
            String XML = "";
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                //check if the stream is enabled
                if (VDGEventStream.isEnabled()) {
                    if (inputLine.indexOf("--" + boundary) == -1) {
                        XML = XML + inputLine;
                    }
                    if (inputLine.indexOf("--" + boundary) > -1 && XML != "") {
                        int beginIndex = XML.indexOf("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                        XML = XML.substring(beginIndex + 38);
                        // System.out.println(XML);
                        System.out.println("\n");
                        Document doc = convertStringToXMLDocument(XML);

                        DateTime eventtime = null;
                        //check if the comming date has the format yyyy-MM-dd HH:mm:ss.SSS
                        if (this.isThisDateValid(doc.getElementsByTagName("time").item(0).getTextContent(),
                                "yyyy-MM-dd HH:mm:ss.SSS")) {
                            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS");
                            //fill the event parameters: eventid, description ...  by parsing the coming XML document
                            eventtime = formatter
                                    .parseDateTime(doc.getElementsByTagName("time").item(0).getTextContent());
                            //initialze value collection object to use it in the addStreamEntry method
                            ValueCollection values = new ValueCollection();

                            long eventidLONG = Long.parseLong(doc.getElementsByTagName("id").item(0).getTextContent());
                            String eventid = doc.getElementsByTagName("id").item(0).getTextContent();
                            String description = doc.getElementsByTagName("name").item(0).getTextContent();
                            String eventvalue = doc.getElementsByTagName("value").item(0).getTextContent();
                            String deviceId = doc.getElementsByTagName("deviceid").item(0).getTextContent();
                            String serverId = doc.getElementsByTagName("serverid").item(0).getTextContent();

                            values.put("eventid", new StringPrimitive(eventid));
                            values.put("eventdescription", new StringPrimitive(description));
                            values.put("eventvalue", new StringPrimitive(eventvalue));
                            values.put("eventtime", new DatetimePrimitive(eventtime));
                            values.put("serverid", new StringPrimitive(serverId));
                            values.put("deviceid", new StringPrimitive(deviceId));
                            values.put("eventidLONG", new LongPrimitive(eventidLONG));

                            if (VDGEventStream == null) {
                                _logger.error("SIP_VDGStreamer_Resource the VDGEventStream obj is null");
                                throw new Exception();
                            }
                            // add the event to the stream 
                            VDGEventStream.addStreamEntry(null, null, null, null, null, values);


                            //check if the event is alarm; if it is add it to the one of the data tables:
                            //PSIM_RedAlarms_DataTable , PSIM_YellowAlarms_DataTable, PSIM_GreenAlarms_DataTable

                            //if the VDG doesn't send the serverId, don't consider it as alarm
                            //because we don't know which station the event comes from
                            if ((serverId != null) && (!serverId.equals("0"))) {

                                for (int i = 0; i < alarmsCodeLength; i++) {
                                    // if event is alarm
                                    if (alarm_codes.get(i).getDescription().equals(description)) 
                                    {
                                        //if the alarm is BayOverstay, simply ignore it
                                        //this code should be updated according to that
                                        //because the BayOverstay is a SENSIT event
                                        //and we are gathering SENSIT events from its source
                                        if (description.indexOf("BayOverstay") > -1) {   
                                            //extract the  "(00:10:00)" from "Bay 2 of SS180 (00:10:00)"                                         
                                            String overstayValue = eventvalue.substring(eventvalue.indexOf("("));
                                            //extract the no Hours from "(00:10:00)"
                                            int noHours = Integer.parseInt(overstayValue.substring(1, 3), 10);
                                            //extract the no Minutes from "(00:10:00)"
                                            int noMinutes = Integer.parseInt(overstayValue.substring(4, 6), 10);
                                            int totalMinutes = noMinutes + (noHours * 60);
                                            //ignore the event and doesn't store it
                                            if (totalMinutes < overStayThreshold)
                                                break;
                                        }
                                        //add alarm to the data table
                                        AddToAlarms(alarm_codes.get(i).getCode(), eventid, description, eventvalue, eventtime, serverId, deviceId);
                                        break;
                                    }
                                }
                            }

                            XML = "";
                        }
                    }
                } else {
                    _logger.error("SIP_VDGStreamer_Resource Error: " + VDGEventStream.getName() + "is disabled, The Extension will stop");
                    StopRecordingEvents();
                }
            }
        } catch (Exception e2) {
            //when the stream is null, it is closed by the service: Stop Events
            if (e2.getMessage().equals("Stream closed"))
                _logger.error("SIP_VDGStreamer_Resource: stream is enforced to close");
            else {
                _logger.error("SIP_VDGStreamer_Resource Error: " + e2.getMessage());
                _logger.error("SIP_VDGStreamer_Resource wait 10 seconds .....");

                try {
                    TimeUnit.SECONDS.sleep(10);
                } catch (InterruptedException var18) {
                    var18.printStackTrace();
                    _logger.error("SIP_VDGStreamer_Resource Error: " + var18.getMessage());
                }

                try {
                    this.StreamEvents();
                } catch (Exception var17) {
                    var17.printStackTrace();
                    _logger.error("SIP_VDGStreamer_Resource Error: " + var17.getMessage());
                }
            }

        }


    }
    //This method is used to add one alaem(row) to one of the data tables:
    //PSIM_GreenAlarms_DataTable, PSIM_YellowAlarms_DataTable or PSIM_RedAlarms_DataTable
    private void AddToAlarms(String AlarmCode, String eventid, String description, String eventvalue, DateTime eventtime, String serverId,
                             String deviceId) throws Exception {

        try {
            //initialize value collection object
            ValueCollection eventValues = new ValueCollection();

            eventValues.SetStringValue("EventID", new StringPrimitive(eventid));
            eventValues.SetStringValue("Description", new StringPrimitive(description));
            eventValues.SetStringValue("EventValue", new StringPrimitive(eventvalue));
            eventValues.SetDateTimeValue("EventTime", new DatetimePrimitive(eventtime));
            eventValues.SetStringValue("ServerID", new StringPrimitive(serverId));
            eventValues.SetStringValue("DeviceID", new StringPrimitive(deviceId));
            
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS");
            Date date = new Date();

            String addToDataTable = "";
            //check if the event is red, yellow or green
            if (AlarmCode.equals("Green"))
                addToDataTable = "PSIM_GreenAlarms_DataTable";
            else if (AlarmCode.equals("Yellow"))
                addToDataTable = "PSIM_YellowAlarms_DataTable";
            else if (AlarmCode.equals("Red"))
                addToDataTable = "PSIM_RedAlarms_DataTable";

            sql = "insert into data_table (entity_id, source_id, entity_key, time, field_values, location, source_type, tags)  "
                    + "values ('" + addToDataTable + "', 'System', '" + eventid + "', '" + dateFormat.format(date) + "','" + eventValues.toJSON() + "', '0.0,0.0,0.0', 'User', '[]' );";

            statement.execute(sql);
            c.commit();


        } catch (Exception e) {

            _logger.error("SIP_VDGStreamer_Resource Exception: Error in writing event as alarm: " + e.getMessage());
            StreamEvents();


        }

    }
    //parse the coming alarm
    private Document convertStringToXMLDocument(String xmlString) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = null;


            builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
            return doc;
        } catch (Exception var5) {
            _logger.error("SIP_VDGStreamer_Resource Exception convertStringToXMLDocument:  " + var5.getMessage());
            return null;
        }
    }
    //not used
    private static void WriteToTxtFile(String xmlData) throws Exception {
        try {
            FileRepositoryThing repo = (FileRepositoryThing) ThingUtilities.findThing("MyCustomFileRepository");
            SystemRepository.CreateTextFile("VDGEvents.txt", "", true);
            SystemRepository.AppendToTextFile("VDGEvents.txt", "\r\n");
            SystemRepository.AppendToTextFile("VDGEvents.txt", xmlData);
        } catch (Exception var2) {
            _logger.error("SIP_VDGStreamer_Resource Exception WriteToTxtFile: " + var2.getMessage());
        }

    }
    //check if the coming alarm has valid date
    public boolean isThisDateValid(String dateToValidate, String dateFromat) {
        try {
            if (dateToValidate == null) {
                return false;
            } else {
                SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
                sdf.setLenient(false);

                try {
                    Date date = sdf.parse(dateToValidate);
                    System.out.println(date);
                    return true;
                } catch (ParseException var5) {
                    var5.printStackTrace();
                    return false;
                }
            }

        } catch (Exception e) {
            _logger.error("SIP_VDGStreamer_Resource Exception: " + e.getMessage());
            return false;
        }
    }

    static {
        SystemRepository = (FileRepositoryThing) EntityUtilities.findEntityDirect("SystemRepository",
                ThingworxRelationshipTypes.Thing);
    }

    //the StopRecordingEvents method is a resource service that used to stop streaming the events
    @ThingworxServiceDefinition(name = "StopRecordingEvents", description = "", category = "", isAllowOverride = false, aspects = {
            "isAsync:false"})
    @ThingworxServiceResult(name = "Result", description = "", baseType = "STRING", aspects = {})
    public String StopRecordingEvents() {
        try {
            if (!isConnected)
                return "SIP_VDGStreamer_Resource: The Connection is already closed";
            else {
                isConnected = false;
                in.close();
                return "SIP_VDGStreamer_Resource: Stop Recording Events";

            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            _logger.error("SIP_VDGStreamer_Resource Exception StopRecordingEvents " + e.getMessage());
            return "SIP_VDGStreamer_Resource Exception StopRecordingEvents " + e.getMessage();
        }

    }
     //the StartRecordingEvents method is a resource service that used to start streaming the events
    //TODO sure that te start is working fine, without opening new connection to the stream
    @ThingworxServiceDefinition(name = "StartRecordingEvents", description = "", category = "", isAllowOverride = false, aspects = {
            "isAsync:false"})
    @ThingworxServiceResult(name = "Result", description = "", baseType = "STRING", aspects = {})
    public String StartRecordingEvents() {
        try {
            if (isConnected)
                return "SIP_VDGStreamer_Resource: The Connection is already opened";
            else {
                isConnected = true;
                _logger.warn("SIP_VDGStreamer_Resource: Start Recording Events");
                StreamEvents();

                return "SIP_VDGStreamer_Resource: Start Recording Events";
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            _logger.error("SIP_VDGStreamer_Resource Exception StartRecordingEvents " + e.getMessage());
        }
        return "SIP_VDGStreamer_Resource: Start Recording Events";
    }
    //set a threshold to consider the event over stay as an alarm; like 30 minutes
    //the service SetOverstayAlarmThreshold should be not uses more
    //since we are gathering SENSIT events from the SENSIT system itself
    @ThingworxServiceDefinition(name = "SetOverstayAlarmThreshold", description = "", category = "", isAllowOverride = false, aspects = {
            "isAsync:false"})
    @ThingworxServiceResult(name = "Result", description = "", baseType = "STRING", aspects = {})
    public String SetOverstayAlarmThreshold(
            @ThingworxServiceParameter(name = "thresholdInMinutes", description = "", baseType = "INTEGER") Integer thresholdInMinutes) {

        try {

            Thing AlarmManagementServices_Thing = (Thing) EntityUtilities.findEntityDirect("PSIM_AlarmManagementServices_Thing",
                    ThingworxRelationshipTypes.Thing);
            if (!AlarmManagementServices_Thing.isRunning()) {
                AlarmManagementServices_Thing.EnableThing();
                AlarmManagementServices_Thing.RestartThing();
            }
            //IPrimitiveType ff = AlarmManagementServices_Thing.getPropertyValue("OverstayMinutesLimit");
            IntegerPrimitive overstayThreshold = new IntegerPrimitive();
            overstayThreshold.setValue(thresholdInMinutes);
            AlarmManagementServices_Thing.setPropertyValue("OverstayMinutesLimit", overstayThreshold);
            overStayThreshold = thresholdInMinutes;

            return "The new Overstay Threshold has been set successfully";

        } catch (Exception e) {
            _logger.error("SIP_VDGStreamer_Resource Exception in SetOverstayThreshold service: " + e.getMessage());
            return "SIP_VDGStreamer_Resource Exception in SetOverstayThreshold service: \" + e.getMessage()";
        }
    }


}
//check if the station is Deployed
//    public Boolean IsDeployed(String serverID)
//    {
//        try
//        {
//            return null;
//        }
//        catch (Exception e)
//        {
//            _logger.error("SIP_VDGStreamer_Resource Error" + e.getMessage());
//        }
//
//    }

//	@ThingworxServiceDefinition(name = "SetParameters", description = "", category = "", isAllowOverride = false, aspects = {
//			"isAsync:false" })
//	@ThingworxServiceResult(name = "Result", description = "", baseType = "STRING", aspects = {})
//	public String SetParameters(
//			@ThingworxServiceParameter(name = "url", description = "", baseType = "STRING") String url,
//			@ThingworxServiceParameter(name = "user_name", description = "", baseType = "STRING") String user_name,
//			@ThingworxServiceParameter(name = "pass", description = "", baseType = "STRING") String pass) {
//        Vdg_apiURL = url;
//        Vdg_userName = user_name;
//        Vdg_Password = pass;
//        _logger.warn("SIP_VDGStreamer_Resource: new Parameters are set, some exception messages will appear until the connection will be established again");
//		StopRecordingEvents();
//		StartRecordingEvents();
//		return "new Parameters are set, some exception messages will appear in the Application log until the connection will be established again";
//	}


